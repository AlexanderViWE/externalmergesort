"""Интерфейс командной строки для внешней сортировки


Примеры:
    python -m sorter sort Examples\txt_Examples\example_1.txt -r -n=3

    python -m sorter sort Examples\txt_Examples\example_2.txt -r -n=3 sort Examples\txt_Examples\example_1.txt
"""

from external_sort import external_merge_sort
import pathlib
import sys
import argparse


def create_parser():
    """Создание парсера"""

    description = '''Сортировка Файлов'''
    parser = argparse.ArgumentParser(description=description)
    subparsers = parser.add_subparsers(help="sub-command help", required=True)
    parsers_sorter = subparsers.add_parser("sort", help="Сортировка массивов")
    parsers_sorter.add_argument("file",
                                help="Флаг сортировки в файле",
                                type=pathlib.Path)
    parsers_sorter.add_argument("-o", "--output",
                                help="Указание файла для сохранения результата",
                                type=pathlib.Path,
                                default=None)
    parsers_sorter.add_argument("-r", "--reverse",
                                help="Отсортировать по не возрастанию",
                                action="store_true",
                                default=False)
    parsers_sorter.add_argument("-k", "--key",
                                help="Ключ для доступа к данным (для *.json, *.csv)",
                                type=str,
                                default=None)
    parsers_sorter.add_argument("-n", "--nflows",
                                help="Число потоков",
                                type=int,
                                default=1,
                                choices=range(1, 20))

    commands = []
    command_args = []
    for i in sys.argv[1:]:
        if i == "sort":
            if len(command_args) != 0:
                commands.append(command_args)
            command_args = []
        command_args.append(i)
    if len(command_args) != 0:
        commands.append(command_args)

    args_to_sort = []
    namespace = argparse.Namespace()
    for cmdline in commands:
        parser.parse_args(cmdline, namespace=namespace)

        file = namespace.file
        if not file.is_file():
            print(f"Файл {file} не найден")
            return

        output = None
        if namespace.output is not None:
            output = namespace.output

        reverse = namespace.reverse

        key = None
        if file.suffix in (".json", ".csv"):
            if not namespace.key:
                print("Отсутствует ключ для сортировки!")
                return
            key = lambda x: x[namespace.key]
        elif namespace.key:
            print("Предупреждение. Для файлов *.txt ключ игнорируется!")

        nflows = namespace.nflows
        # bsize = None

        args_to_sort.append(
            (file, output, reverse, key, nflows)
        )

    for args in args_to_sort:
        print(f"Sorting '{args[0]}'...")
        try:
            external_merge_sort(*args)
        except FileNotFoundError as err:
            print(f"При сортировке файла произошла ошибка:\n{err}")
        print("Finish sorting.")


def main():
    """Точка входа"""
    create_parser()


if __name__ == '__main__':
    main()
