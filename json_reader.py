"""Чтение json файлов

Пример:
    with open(f"file_name", "r") as f:
        reader = JsonArrayReader(f)
        for row in reader:
            print(row)

Выжно:
    Текстовые значения в json файлах не должны содержать символ ".

    Формат необходимого json файла:
        [obj, ...]
    где obj объект, строка, число или массив
"""

import json
from typing import TextIO


class JsonArrayReader:
    """Сканер json файлов
    """

    def __init__(self, file: TextIO):
        """
        :param file: Открытый файл
        """
        self.file = file

    def __iter__(self):
        input_file = self.file
        input_file.seek(0)

        tmp = input_file.read(1)
        if tmp != "[":
            raise ValueError(f"{tmp}")

        pos = input_file.tell()
        char = input_file.read(1)
        while char in (" ", "\n", '\t'):
            pos = input_file.tell()
            char = input_file.read(1)
        if char == "]":
            # Список пуст
            return iter([])
        input_file.seek(pos)

        if char == "{":
            # Список заполнен объектами
            iter_ = _json_search_object_pattern(input_file, "{", "}")

        elif char == "[":
            # Список заполнен списками
            iter_ = _json_search_object_pattern(input_file, "[", "]")

        else:
            # Список заполнен примитивами (float, str, int)
            iter_ = _json_pattern_search_primitive_objects(input_file)

        return iter(iter_)


def _json_search_object_pattern(input_file: TextIO,
                                opening_brace: str = "{",
                                closing_brace: str = "}") -> object:
    """Чтение сложных объектов: массивов, объектов
    :param input_file: Файл json
    :param opening_brace: Открывающая скобка
    :param closing_brace: Открывающая скобка
    :return: Объект из массива json
    """
    is_str_item = False
    is_object = False
    counter_o = 0

    left_pos = input_file.tell()
    char_counter = 0

    char = input_file.read(1)
    assert char == opening_brace, char

    while char:
        if char_counter is not None:
            char_counter += 1

        if char == '"':
            is_str_item = not is_str_item

        if char == opening_brace and not is_str_item:
            is_object = True
            counter_o += 1

        elif char == closing_brace and not is_str_item and is_object:
            assert counter_o != 0, counter_o
            counter_o -= 1
            if counter_o == 0:
                if left_pos is not None and char_counter is not None:
                    input_file.seek(left_pos)
                    tmp = input_file.read(char_counter).strip()
                    # print(tmp, end="")
                    yield json.loads(tmp)

                char_counter = 0
                # counter_o = None
                is_object = False

                tmp = input_file.read(1)
                if tmp == ",":
                    left_pos = input_file.tell()

        char = input_file.read(1)


def _json_pattern_search_primitive_objects(input_file: TextIO) -> object:
    """Чтение простых объектов: строк, чисел
    :param input_file: Файл json
    :return: Объект из массива json
    """
    left_pos = input_file.tell()
    char_counter = 0

    is_str_item = False
    char = input_file.read(1)
    while char:
        if char == '"':
            is_str_item = not is_str_item

        if char in (",", "]") and not is_str_item:
            input_file.seek(left_pos)
            tmp = input_file.read(char_counter).strip()
            # print(tmp)
            yield json.loads(tmp)

            char_counter = 0
            input_file.read(1)
            left_pos = input_file.tell()
        else:
            char_counter += 1
        char = input_file.read(1)
