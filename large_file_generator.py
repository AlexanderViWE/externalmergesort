import random
import json
from pathlib import Path


def generate_big_txt(path: Path, count=10_000):
    path.touch()

    with path.open("w", encoding="UTF-8") as f:
        for i in range(count):
            num = random.randint(0, count)
            f.write(f"{num}\n")


def generate_big_scv(path: Path, count=10_000):
    path.touch()

    with path.open("w", encoding="UTF-8") as f:
        f.write(f"\"\",\"x\"\n")
        for i in range(count):
            num = random.randint(0, count)
            f.write(f"\"{i}\",{num}\n")


def generate_big_json(path: Path, count=10_000):
    path.touch()

    with path.open("w", encoding="UTF-8") as f:
        f.write(f"[\n")
        for i in range(count):
            num = random.randint(0, count)
            obj = {"id": i, "val": num}
            f.write(f"{json.dumps(obj, separators=(',', ':'))}")
            if i + 1 != count:
                f.write(",")
            f.write("\n")
        f.write(f"]")


def main():
    path = Path(r"Examples/csv_Examples/example_1.csv")
    generate_big_scv(path, 1000)


if __name__ == '__main__':
    main()
