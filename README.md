# Внешняя сортировка слиянием

## Описание 
Приложения с интерфейсом командной строки для сортировки 
данных в json, csv, txt файлах без загрузки всего файла.

Поддерживаемые типы данных утилитой: str

## Пример использования утилиты
```
python -m sorter sort Examples/txt_Examples/example_1.txt

python -m sorter sort Examples/csv_Examples/example_1.csv --key=x

python -m sorter sort Examples/json_Examples/example_1.json --key=name
```

