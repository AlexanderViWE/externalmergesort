"""Преобразование отсортированных файлов к исходному расширению
"""

from pathlib import Path
import json
import csv


# def txt_restoring(res_file: Path, new_file: Path, *, encoding="UTF-8") -> None:
#    """Преобразование отсортированнаго файлов к txt
#    """
#    with new_file.open("w", encoding=encoding) as output_f:
#        with res_file.open("r", encoding=encoding) as input_f:
#            buf = input_f.readline()
#            while buf:
#                output_f.write(buf)


def csv_restoring(res_file: Path, old_file: Path, new_file: Path, *, encoding: str = "UTF-8") -> None:
    """Преобразование отсортированнаго файлов к csv
    :param res_file: Отсортированный файл
    :param old_file: Исходный файл (Необходим для чтения заголовока)  TODO: А если заголовок отсутствует?
    :param new_file: Файл для записи отсортированных данных
    :param encoding: Кодировка
    """
    with old_file.open("r") as input_file:
        reader = csv.reader(input_file)
        headers = next(reader)

    head = []
    for column_name in headers:
        head.append(f'"{column_name}"')
    head = ",".join(head) + '\n'

    with new_file.open("w", encoding=encoding) as output_f, \
            res_file.open("r", encoding=encoding) as input_f:
        output_f.write(head)

        buf = input_f.readline().replace("'", '"')
        while buf:
            buf = json.loads(buf)
            row = []
            for i in headers:
                row.append(buf[i])
            output_f.write(",".join(row) + "\n")

            buf = input_f.readline().replace("'", '"')


def json_restoring(res_file: Path, new_file: Path, *, encoding: str = "UTF-8") -> None:
    """Преобразование отсортированнаго файлов к json
    :param res_file: Отсортированный файл
    :param new_file: Файл для записи отсортированных данных
    :param encoding: Кодировка
    """
    with new_file.open("w", encoding=encoding) as output_f, \
            res_file.open("r", encoding=encoding) as input_f:
        output_f.write("[\n")

        buf = input_f.readline().strip()
        while buf:
            output_f.write(buf)
            buf = input_f.readline().strip()
            if buf:
                output_f.write(",")
            output_f.write("\n")
        output_f.write("]")
