"""Подготовка файлов для сортировки
"""

from pathlib import Path
import json
import csv
from json_reader import JsonArrayReader


# Нужно поэлементно считать входной файл и разбить его
# на серии, где элементы в серии отсортированы по неубыванию.

# TODO: Реализовать сравнение чечез cmp()
#  + Переписать функции, т.к. они имеют общий код.


def txt_files_preparation(src: Path, sub_files, key, reverse: bool = False, *, encoding="UTF-8") -> int:
    """Подготовка txt файлов для сортировки
    :param src: Исходный файл
    :param sub_files: Дополнительные файлы для сортировки
    :param key: Функция для сравнения
    :param reverse: Флаг определяющий вариант сортировки
    :param encoding: Кодировка файла
    :return: Число серий записанных в sub_files.
    """

    group_counter = 0
    sub_files = [file.open("w", encoding=encoding) for file in sub_files]

    with src.open("r", encoding=encoding) as input_file:
        file_number = 0
        cur_file = sub_files[file_number]

        buf = input_file.readline()
        cur_file.write(f"{buf}\n")

        while True:
            next_element = input_file.readline()
            if not next_element:
                group_counter += 1
                break

            comparison = key(next_element) < key(buf)
            if reverse:  # XOR ?
                comparison = not comparison

            if comparison:
                group_counter += 1

                file_number = (file_number + 1) % len(sub_files)
                cur_file = sub_files[file_number]

                # Если выходной файл не пуст, то записать разделитель серий
                if cur_file.tell() != 0:
                    cur_file.write("'\n")

            cur_file.write(f"{next_element.strip()}\n")
            buf = next_element

    for i in sub_files:
        i.close()

    return group_counter


def csv_files_preparation(src: Path, sub_files, key, reverse: bool = False, *, encoding="UTF-8") -> int:
    """Подготовка csv файлов для сортировки
    :param src: Исходный файл
    :param sub_files: Дополнительные файлы для сортировки
    :param key: Функция для сравнения
    :param reverse: Флаг определяющий вариант сортировки
    :param encoding: Кодировка файла
    :return: Число серий
    """

    group_counter = 0
    sub_files = [file.open("w", encoding=encoding) for file in sub_files]

    with src.open("r", encoding=encoding) as input_file:
        reader = csv.DictReader(input_file)

        file_number = 0
        cur_file = sub_files[file_number]

        buf = next(reader)
        cur_file.write(f"{buf}\n")

        for line in reader:
            next_element = line
            if not next_element:
                group_counter += 1
                break

            comparison = key(next_element) < key(buf)
            if reverse:
                comparison = not comparison

            if comparison:
                group_counter += 1

                file_number = (file_number + 1) % len(sub_files)
                cur_file = sub_files[file_number]

                if cur_file.tell() != 0:
                    cur_file.write("'\n")

            cur_file.write(f"{next_element}\n")
            buf = next_element

    for i in sub_files:
        i.close()

    return group_counter


def json_files_preparation(src: Path, sub_files, key, reverse: bool = False, *, encoding="UTF-8") -> int:
    """Подготовка json файлов для сортировки
    :param src: Исходный файл
    :param sub_files: Дополнительные файлы для сортировки
    :param key: Функция для сравнения
    :param reverse: Флаг определяющий вариант сортировки
    :param encoding: Кодировка файла
    :return: Число серий
    """

    group_counter = 0
    sub_files = [file.open("w", encoding=encoding) for file in sub_files]

    with src.open("r", encoding=encoding) as input_file:
        reader = iter(JsonArrayReader(input_file))

        file_index = 0
        cur_file = sub_files[file_index]

        buf = next(reader)
        cur_file.write(f"{json.dumps(buf, separators=(',', ':'))}\n")

        for line in reader:
            next_element = line
            if not next_element:
                group_counter += 1
                break

            comparison = key(next_element) < key(buf)
            if reverse:
                comparison = not comparison

            if comparison:
                group_counter += 1

                file_index = (file_index + 1) % len(sub_files)
                cur_file = sub_files[file_index]

                if cur_file.tell() != 0:
                    cur_file.write("'\n")

            cur_file.write(f"{json.dumps(next_element, separators=(',', ':'))}\n")
            buf = next_element

    for i in sub_files:
        i.close()

    return group_counter
