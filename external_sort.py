"""Двухпутевое сбалансированное слияние

Ограничения:
    *.csv, *.json, *.txt файлы

    В строковых переменных json файлов не должно быть символа "

Термины:
    Двухпутевое слияние – это сортировка, в которой данные распределяются
    на два вспомогательных файла.

    Сбалансированное слияние – это естественное слияние, у которого после
    фазы распределения количество серий во вспомогательных файлах
    отличается друг от друга не более чем на единицу.
"""

import json
import pathlib
import threading

from typing import Union, Optional, Callable, Tuple
from preparing_files import csv_files_preparation, csv_restoring, \
    json_files_preparation, json_restoring, txt_files_preparation


def external_merge_sort(src: Union[str, pathlib.Path],
                        output: Optional[Union[str, pathlib.Path]] = None,
                        reverse: bool = False,
                        key: Optional[Callable[[object], object]] = None,
                        nflows: int = 1,
                        bsize: Optional[int] = None,
                        encoding: str = "UTF-8") -> None:
    """Двухпутевое сбалансированное слияние

    :param src: Исходный файл.
    :param output: Выходной файл, если не указан файл src сортируется на месте.
    :param reverse: Флаг определяющий вариант сортировки:
                    False - по неубыванию;
                    True - по невозрастанию.
    :param key: Функция, вычисляющая значение, на основе которого будет производится сортировка.
                Должна принимать один аргумент и возвращать значение.
    :param nflows: Количество потоков.
    :param bsize: TODO: Размер блока, хранимого в памяти.
    :param encoding: Кодировка файла.
    """

    # --- Обработка параметров функции ---
    # src
    src = pathlib.Path(src).resolve()
    if not src.is_file():
        raise FileNotFoundError(f"{src}")

    # key
    if key is None:
        key = lambda x: x

    # output
    if output is None:
        output = src.with_name(f"{src.stem}_sorted{src.suffix}")
    else:
        output = pathlib.Path(output)

    # nflows
    if not isinstance(nflows, int):
        raise TypeError(f"nflows must be int, not {type(nflows)}")

    # bsize
    # ...

    # --- Создание дополнительных файлов ---
    sub_files = []
    for i in range(2):
        tmp_file = output.with_name(f"tmp_{i}_{src.stem}.txt")
        # TODO: А если файл уже существует?
        #  Придумать способ генерации уникальных имен файлов.
        tmp_file.touch(exist_ok=False)
        sub_files.append(tmp_file)
    sub_files = tuple(sub_files)

    # Преобразование данных в удобный для сортировки формат.
    #
    # Исходный файл разбивается на серии, а серии записываются в
    # дополнительные файлы. Элементы в серии отсортированы
    # по неубыванию.
    try:
        # group_counter - наибольшее количество серий в дополнительных файлах.

        if src.suffix == ".json":
            group_counter = json_files_preparation(src, sub_files, key, reverse, encoding=encoding)
            cmp = _get_cmp_func(json_value(key), reverse)

        elif src.suffix == ".csv":
            group_counter = csv_files_preparation(src, sub_files, key, reverse, encoding=encoding)
            cmp = _get_cmp_func(csv_value(key), reverse)

        elif src.suffix == ".txt":
            group_counter = txt_files_preparation(src, sub_files, key, reverse, encoding=encoding)
            cmp = _get_cmp_func(key, reverse)

        else:
            raise UnsupportedFileExtension(f"*{src.suffix} files are not supported.")

    except FileNotFoundError as err:
        for i in sub_files:
            i.unlink()
        raise err

    # Создать временный файл с результатами. В нем будут
    # записаны результаты слияния серий.
    res_file = output.parent / f"tmp_res_{src.stem}.txt"
    res_file.touch(exist_ok=False)

    # FIXME: Из за того, что функции преобразования данных,
    #  также разбивают исходный файл, нужен этот кусок кода.
    #  Как вариант, убрать разбиение в этих функциях.
    _merging(sub_files, res_file, cmp)

    # --- Сортировка ---
    # Пока не останется одна серия:
    while group_counter != 1:
        # Разбить входной файл по сериям.
        _splitting(res_file, sub_files)
        # Вычислить наибольшее количество серий в дополнительных файлах.
        group_counter = sum(divmod(group_counter, 2))

        # Слить файлы в один.
        # 1) Взять по одной серии из файла.
        # 2) Объединить серии, чтобы элементы были отсортированы.
        if nflows == 1:
            _merging(sub_files, res_file, cmp)
        else:
            nflows = min(group_counter, nflows)
            _multi_merging(sub_files, res_file, cmp, nflows, group_counter)

    # Преобразуем отсортированный "массив" к формату исходного файла.
    if src.suffix == ".json":
        output.touch()
        json_restoring(res_file, output, encoding=encoding)
        res_file.unlink()

    elif src.suffix == ".csv":
        output.touch()
        csv_restoring(res_file, src, output, encoding=encoding)
        res_file.unlink()

    elif src.suffix == ".txt":
        res_file.replace(output)

    else:
        # Никогда не должен вызоваться.
        raise Exception()

    # Удаляем временные файлы
    for i in sub_files:
        i.unlink()


def _splitting(file: pathlib.Path,
               sub_files: Tuple,
               encoding: str = "UTF-8") -> None:
    """Разбиение серий по файлам.

    :param file: Файл с сериями.
    :param sub_files: Файлы для серий.
    :param encoding: Кодировка файла.
    """
    sub_files = [file.open("w", encoding=encoding) for file in sub_files]
    file_number = 0

    with file.open("r", encoding=encoding) as input_file:
        cur_file = sub_files[file_number]

        buf = input_file.readline()

        # Пока не конец файла
        while buf:
            # Если серия элементов закончилась, то выбрать другой выходной файл.
            if buf.strip() == "'":
                file_number = (file_number + 1) % len(sub_files)
                cur_file = sub_files[file_number]

                # Записать разделитель серий элементов
                if cur_file.tell() != 0:
                    cur_file.write(buf)  # buf == "'\n"
            else:
                cur_file.write(buf)

            buf = input_file.readline()

    for i in sub_files:
        i.close()


def _merging(sub_files: Tuple,
             file: pathlib.Path,
             cmp: Callable[[object, object], bool],
             encoding: str = "UTF-8") -> None:
    """Слияние серий
    :param sub_files: Файлы с сериями
    :param file: Файл для слияния
    :param cmp: Функция сравнения
    :param encoding: Кодировка файла.
    """

    assert len(sub_files) == 2, "Пока поддерживается только слияние 2-х файлов"
    first, second = sub_files

    with file.open("w", encoding=encoding) as output_f, \
            first.open("r", encoding=encoding) as first_input, \
            second.open("r", encoding=encoding) as second_input:

        first_elem = first_input.readline()
        second_elem = second_input.readline()

        while first_elem or second_elem:
            if output_f.tell() != 0:
                output_f.write("'\n")

            while True:
                # Слияение двух серий
                if (first_elem.strip() == "'" or second_elem.strip() == "'" or
                        not first_elem or not second_elem):
                    break

                if cmp(first_elem, second_elem):
                    output_f.write(second_elem)
                    second_elem = second_input.readline()
                else:
                    output_f.write(first_elem)
                    first_elem = first_input.readline()

            while first_elem.strip() != "'" and first_elem:
                # Запись оставшихся элементов сирии
                output_f.write(f"{first_elem.strip()}\n")
                first_elem = first_input.readline()

            while second_elem.strip() != "'" and second_elem:
                # Запись оставшихся элементов сирии
                output_f.write(f"{second_elem.strip()}\n")
                second_elem = second_input.readline()

            first_elem = first_input.readline()
            second_elem = second_input.readline()


def _get_cmp_func(key: Callable[[object], object],
                  reverse: bool = False) -> Callable[[object, object], bool]:
    """Создание функции сравнения

    :param key: Функция, вычисляющая значение
    :param reverse: Флаг определяющий вариант сравнения
    :return: Функция сравнения
    """

    def cmp(first, second) -> bool:
        """Функция сравнения
        :param first: Первые элемент
        :param second: Второй элемент
        :return: Результат сравнения
        """
        comparison = key(first) > key(second)
        if reverse:
            comparison = not comparison
        return comparison

    return cmp


def json_value(func: Callable[[str], object]) -> Callable:
    """Декоратор. Возвращает значение по ключу для строки json файла

    :param func: Декорируемая функция
    :return: Декорированная функция
    """

    def new_func(string):
        """Преобразование строки в объект"""
        obj = json.loads(string)
        return func(obj)

    return new_func


def csv_value(func: Callable[[str], object]) -> Callable:
    """Декоратор. Возвращает значение по ключу для строки scv файла

    :param func: Декорируемая функция
    :return: Декорированная функция
    """

    def new_func(string):
        """Преобразование строки в объект"""
        obj = json.loads(string.replace("'", '"'))
        return func(obj)

    return new_func


class UnsupportedFileExtension(Exception):
    """Неподдерживаемое расширение файла"""


# -----------------------------------------------
# А многопоточность даст ускорение?
# GIL...
# Последовательное чтение SSD...

def _multi_merging(sub_files: Tuple,
                   file: pathlib.Path,
                   cmp: Callable[[object, object], bool],
                   nflows: int,
                   group_number: int) -> None:
    """Многопоточное слияние
    :param sub_files: Доп файлы
    :param file: Файл для слияния
    :param cmp: Функция сравнения
    :param nflows: Число потоков
    :param group_number: Число серий
    """
    first, second = sub_files

    tmp_files = []
    for i in range(nflows):
        tmp = file.parent / f"filepart_{i}.txt"
        tmp.touch()
        tmp_files.append(tmp)

    # Поиск количества серий для потоков
    group_sizes = [group_number // nflows for _ in range(nflows)]
    for i in range(group_number % nflows):
        group_sizes[-1 - i] += 1
    size_next_group = group_sizes.pop(0)

    flows = []
    flow_number = 0  # Счетчик запущенных потоков
    counter = 0  # Счетчик серий
    with first.open() as first_f, \
            second.open() as second_f:
        first_f_pos_0 = first_f.tell()
        second_f_pos_0 = second_f.tell()

        first_elem = first_f.readline()
        second_elem = second_f.readline()
        while first_elem or second_elem:
            while first_elem.strip() != "'" and first_elem:
                first_elem = first_f.readline()
            while second_elem.strip() != "'" and second_elem:
                second_elem = second_f.readline()

            counter += 1

            if size_next_group == counter:
                tmp = threading.Thread(target=_flow_merge,
                                       args=(tmp_files[flow_number],
                                             sub_files,
                                             (first_f_pos_0, second_f_pos_0),
                                             size_next_group,
                                             cmp))
                flows.append(tmp)
                tmp.start()

                counter = 0
                flow_number += 1
                if len(group_sizes) != 0:
                    size_next_group = group_sizes.pop(0)

                first_f_pos_0 = first_f.tell()
                second_f_pos_0 = second_f.tell()

            first_elem = first_f.readline()
            second_elem = second_f.readline()

    # Ожидание завершения потоков
    for i in flows:
        i.join()

    with file.open("w", encoding="UTF-8") as output_f:
        # Объединение файлов
        for file_part in tmp_files:
            with file_part.open("r", encoding="UTF-8") as input_f:
                buf = input_f.readline()

                if output_f.tell() != 0 and buf:
                    output_f.write("'\n")

                while buf:
                    output_f.write(buf)
                    buf = input_f.readline()
            file_part.unlink()


def _flow_merge(file: pathlib.Path,
                sub_files: Tuple,
                starting_positions: Tuple[int, int],
                groups_number: int,
                cmp: Callable[[object, object], bool]) -> None:
    """Функция запускаемая как поток
    :param file: Файл для слияния
    :param sub_files: Доп файлы
    :param starting_positions: Позиции в доп файлах с которых начать чтение
    :param groups_number: Число серий для слияния
    :param cmp: Функция сравнения
    """
    first, second = sub_files

    group_count = 0
    with file.open("w") as output_f, \
            first.open("r") as first_input, \
            second.open("r") as second_input:
        first_input.seek(starting_positions[0])
        second_input.seek(starting_positions[1])

        first_elem = first_input.readline()
        second_elem = second_input.readline()

        while first_elem or second_elem:
            if output_f.tell() != 0:
                output_f.write("'\n")

            while True:
                # Слияение двух серий
                if (first_elem.strip() == "'" or second_elem.strip() == "'" or
                        not first_elem or not second_elem):
                    break

                if cmp(first_elem, second_elem):
                    output_f.write(second_elem)
                    second_elem = second_input.readline()
                else:
                    output_f.write(first_elem)
                    first_elem = first_input.readline()

            while first_elem.strip() != "'" and first_elem:
                # Запись оставшихся элементов сирии
                output_f.write(f"{first_elem.strip()}\n")
                first_elem = first_input.readline()

            while second_elem.strip() != "'" and second_elem:
                # Запись оставшихся элементов сирии
                output_f.write(f"{second_elem.strip()}\n")
                second_elem = second_input.readline()

            group_count = group_count + 1
            if group_count == groups_number:
                break

            first_elem = first_input.readline()
            second_elem = second_input.readline()
